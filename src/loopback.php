<?php

/**
* Author: Joseph Barlan
* Date: 05/03/2015
* ID: cs290, Assignment 4 part1
*/

// Display the Error if encountered
error_reporting(E_ALL);
ini_set('display_errors', 'ON');

//create a key/value array 
$formJSON = array('Type' => null, 'parameters' => null);

//get the request method
$formJSON['Type'] = $_SERVER['REQUEST_METHOD'];

//get the key/value pairs
$formJSON['parameters'] = ($_SERVER['REQUEST_METHOD'] == 'GET' ? $_GET : $_POST);

//check if parameters are empty
if(empty($formJSON['parameters']))
   $formJSON['parameters'] = null;

//encode the array into JSON
echo json_encode($formJSON);

?>