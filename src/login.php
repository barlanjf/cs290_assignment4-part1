<?php
/**
* Author: Joseph Barlan
* Date: 05/03/2015
* ID: cs290, Assignment 4 part1
*/

//find existing session
session_start();

if(isset($_GET['action']))
{
    //logout and kill the session
    if($_GET['action'] == 'end')
    {
        session_destroy();
        header("Location: login.php");
    }

}
else if(isset($_SESSION['username']) )
{
    echo "You are already signed on as $_SESSION[username]. </br>";
    echo "<a href='login.php?action=end'> LOG OUT </a>";
}
else 
{
    echo '  <!DOCTYPE html>
            <html>
            <head>
            <meta charset="utf-8">
            <title>Login</title>
            </head>
            <body>
                <form action="content1.php" method="POST" name="loginForm">
                    Username: <input type="text" name="username">
                    <input type="submit" value="Login">
                </form>
            </body>
            </html>
            ';  
}
?>