<?php

/**
* Author: Joseph Barlan
* Date: 05/03/2015
* ID: cs290, Assignment 4 part1
*/

// Display the Error if encountered
error_reporting(E_ALL);
ini_set('display_errors', 'ON');

echo   '<!DOCTYPE html>
        <html>
        <head>
        <meta charset="utf-8">
        <title>multtable.php</title>
        </head>
        <body>';

// the parameters passed via the URL in a GET request
$paramGET = ['min-multiplicand', 
             'max-multiplicand', 
             'min-multiplier', 
             'max-multiplier'];
/**
 * This function Validated the GET parameters
 */
function isValid($array)
{
    $isValid = true;
    
    foreach ($array as $key) 
    {
        //check for missing parameters
        if(!isset($_GET[$key]))
        {
            echo "Parameter " . $key . " is missing </br>";
            $isValid = false;
        }
        
        //check for valid integers
        if($isValid === true && !is_numeric($_GET[$key]))
        {
            echo "Parameter " . $key . " value is not a valid integer </br>";
            return false;
        }   
    }
    
    if($isValid === false)
        return false;
    
    // check for min > max
    if( $_GET['min-multiplicand'] >  $_GET['max-multiplicand'])
    {
        echo "Error: min-multiplicand is higher than max-multiplicand </br>";
        return false;
    }
    
    if(  $_GET['min-multiplier'] > $_GET['max-multiplier'])
    {
        echo "Error: min-multiplier is higher than max-multiplier </br>";
        return false;
    }
    
    // all validated
    return true;
}


if(isValid($paramGET)) 
{
    //build the multiplication table
    echo "<table id='multtable'>";
    
        //start and end of first row
        $heightStart = $_GET['min-multiplicand']-1;
        $heightEnd = $_GET['max-multiplicand'];

        //start and end of first column
        $widthStart = $_GET['min-multiplier']-1;
        $widthEnd = $_GET['max-multiplier'];


        for($i = $heightStart; $i <= $heightEnd; ++$i) 
        {
            echo '<tr>';
            for($j = $widthStart; $j <= $widthEnd; ++$j)
            {
                //check for upper left cell
                if( $i == $heightStart && $j == $widthStart )
                    echo '<th></th>';
                // check if first header column
                else if ($i == $heightStart)
                    echo "<th class='multHeader'>" . $j . "</th>";
                //check if first row header
                else if ($j == $widthStart)
                    echo "<th class='multHeader'>" . $i . "</th>";
                //otherwise print i*j result
                else
                    echo "<td>" . $i * $j . "</td>";
            }
            echo '</tr>';

        }
    
    
    echo "</table>";
}


echo '  </body>
        </html>';


?>
    
