<?php
/**
* Author: Joseph Barlan
* Date: 05/03/2015
* ID: cs290, Assignment 4 part1
*/
echo   '<!DOCTYPE html>
        <html>
        <head>
        <meta charset="utf-8">
        <title>multtable.php</title>
        </head>
        <body>';


session_start();

/********* MAIN ********************************************/

//check if user has alread logged in before
if(isset($_SESSION['username']) )
{
    if (isset($_POST['username'])) // is the user coming from login page again?
    {
        if($_SESSION['username'] === $_POST['username'])
        {
            visitedCountMessage();
        }
        else
        {
            firstLogin();
        }
    }
    else // the user came from another page, but logged in before
        visitedCountMessage();
}
else
{
    firstLogin(); //user first time loggin in with this username
}
    


/*********************** FUNCTIONS ****************************************/
function firstLogin() 
{
    // check if username provided, if first time logging in
    if (isset($_POST['username']))
    {
        
        $username = $_POST['username'];
        if($username === "" || $username === null)
        {
            noUserNameFound();
        }
        else
        {
            // check if there is existing session
            if(!isset($_SESSION['username']))
            {
                $_SESSION['countLogIn'] = 0; //if not, create new session
                $_SESSION['username'] = $username;
            }

            visitedCountMessage();
        }
    }
    else
        noUsernameFound();
}

function noUserNameFound()
{
    echo "A username must be entered. Click <a href='login.php'>here</a> to return to the login screen. </br>";
}

function visitedCountMessage()
{
    echo "Hello $_SESSION[username], you have visited this page $_SESSION[countLogIn]. ";
    echo "Click <a href='login.php?action=end'> Here </a> to log out. </br>";
    echo "Click <a href='content2.php'> here </a> to go to Content 2. </br>";
    $_SESSION['countLogIn']++;
}


/* End HTML */
echo '  </body>
        </html>';

?>